<?php

use yii\db\Migration;

/**
 * Class m181015_104857_create_model_product
 */
class m181015_104857_create_model_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(120)->notNull(),
            'category' => $this->string(55),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
